import {Container} from 'react-bootstrap'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import AppNavBar from './components/AppNavBar';
/*import Banner from './components/Banner';
import Highlights from './components/Highlights';*/
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Courses from './pages/Courses'
import Error from './pages/Error'
import './App.css';

function App() {
  return(
    <>
    <Router>
      <AppNavBar/>  
      <Container>
        <Routes >
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/Courses" element={<Courses/>}/>
          <Route exact path="/Register" element={<Register/>}/>
          <Route exact path="/Login" element={<Login/>}/>
          <Route exact path="/Logout" element={<Logout/>}/>
          <Route exact path="*" element={<Error/>}/>
        </Routes>
      </Container>
      </Router>
    </>
    )
}

export default App;
