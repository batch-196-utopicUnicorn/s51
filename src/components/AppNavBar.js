import {Navbar, Container, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import {useState} from 'react'

export default function AppNavBar(){

  // state hook to store the user information stored in the login page
  const [user, setUser] = useState(localStorage.getItem('email'));
  console.log(user)

	return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to='/'>Batch-196 Booking App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">

            <Nav.Link as={Link} to='/'>Home</Nav.Link>
            <Nav.Link as={Link} to='/Courses'>Courses</Nav.Link>
          {
            (user !== null) ?
            <Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
            :
            <>
              <Nav.Link as={Link} to='/Login'>Login</Nav.Link>
              <Nav.Link as={Link} to='/Register'>Register</Nav.Link>
            </>
          }
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
};